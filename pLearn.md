# Read even more than GSoC you C***

- Read about multimodals.
  - [This](https://vimeo.com/305210831) is a conference talk paper helps explains things that how machine learning is done on multimodals.
  - Wikipedia page on [Multimodality](https://en.wikipedia.org/wiki/Multimodality).

- Computation modelling about multimodals.
  - This [Paul Guy](http://www.cs.cmu.edu/~pliang/) has some great work on multimodality. He gave the talk ( the link I posted above as the 1st point ).

## Some definitions

- **Acoustic**
  - Acoustics is the branch of physics that deals with the study of all mechanical waves in gases, liquids, and solids including topics such as vibration, sound, ultrasound and infrasound.

- **Baseline Models**
  - A baseline is the result of a very basic model/solution. You generally create a baseline and then try to make more complex solutions in order to get a better result. If you achieve a better score than the baseline, it is good ( *copied from [here](https://datascience.stackexchange.com/questions/30912/what-does-baseline-mean-in-the-context-of-machine-learning)* ). You can just Google to find more good definitions.
  - People use different baselines, here they have used a different baseline, see that section, please!!!!!!!.

## About the Papers

### Seq2Seq2Sentiment:Multimodal Sequence to Sequence Models for Sentiment Analysis

#### Abstract and Introduction Part

- So this paper first of all tells us that, making machine learning models used to effectively make representations ([Representation Learning](https://en.wikipedia.org/wiki/Feature_learning)) is a hot topic.
- This paper uses 2 approaches for unsupervised learning:
  - Seq2Seq Modality Translation Model.
  - Hierarchical Seq2Seq Modality Translation Model.

- They also claim that their result regarding the multimodal sentiment analysis on the CMU-MOSEI dataset outperforms the baselines and their model has learned some great intuitive things. `NICE!!!!`

- People have been doing sentiment analysis using mostly textual analysis, lately people have been using **visual and acoustic modalities**.
  - We have lot of multimodal data on YouTube, Facebook etc.
  - Neural Networks on this multimodal data have yeild promising results.
  - We tend to do `Unsupervised Learning` here.

#### Baselines

- Here an LSTM model is used 3 different ways:
  - **Unimodal Domain:** Just use one modality to run the sentiment analysis. They have used each modality one-by-one and each score is tabulated.
  - **Bimodal Domain:** Used any two pair of modalities and did sentiment analysis. Each pair is used one-by-one and the results tabulated.
  - **Trimodal Domain:** Used all the modalities together and did sentiment analysis.

- They did this type of baseline as they wanted to remove any structural changes so that if their model exceeds the baseline it is due to only because its representations.

- From the different baselines models the paper concluded that **text modality is the most prominent in differentiating the data**.

#### Overall Setup

- `66.67%` is the training data.
- `15.15%` is the held-out set for validation.
- `33.33%` is used for testing.
- The comparison with the baseline is done using `precision`, `recall` and `F1` scores.
