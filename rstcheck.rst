=======
Bounces
=======

An important feature of Mailman is automatic bounce processing.

Bounce Attributes and Definitions
=================================

For understanding how Mailman handles bounces, three things need to be understood.

- ``How bounces are generated.``
    - Detailed inforamtion will be available on this `Wikipedia page`_.
    Basically it is an automated message by the MTA informing the message was not delivered
    succesfully due to some reason.
- How mailman extracts data from the bounces.
    - Here we use a concept called `Verp` which helps us to identify as to which member
    was responsible for generating the bounce.
    - You can understand the reason as to why we need this kind of system at all
    in the first place.
- What action does it take using the extracted data.

Bounce processing is handled in different files:

* ``mailman/src/mailman/app/bounces.py`` does the application level of bounce processing. It has various functions for ``creation of bounce event``, ``Verp parser``, ``probe sending``.
* ``mailman/src/mailman/interfaces/bounce.py`` is the interface for the bounce model.
* ``mailman/src/mailman/model/bounce.py`` is the model file containing various definitions of classes like ``BounceEvent``, ``BounceProcessor`` and other functions.
* ``mailman/src/mailman/runners/bounce.py`` is the main runner file containing the ``BounceRunner`` class. This file calls the other functions and does the extractions and partial taking action part.

Bounces, or message rejection
=============================

Mailman can bounce messages back to the original sender.  This is essentially
equivalent to rejecting the message with notification.  Mailing lists can
bounce a message with an optional error message.

    >>> mlist = create_list('ant@example.com')

Any message can be bounced.

    >>> msg = message_from_string("""\
    ... To: ant@example.com
    ... From: aperson@example.com
    ... Subject: Something important
    ...
    ... I sometimes say something important.
    ... """)

Bounce a message by passing in the original message, and an optional error
message.  The bounced message ends up in the virgin queue, awaiting sending
to the original message author.

    >>> from mailman.app.bounces import bounce_message
    >>> bounce_message(mlist, msg)
    >>> from mailman.testing.helpers import get_queue_messages
    >>> items = get_queue_messages('virgin')
    >>> len(items)
    1
    >>> print(items[0].msg.as_string())
    Subject: Something important
    From: ant-owner@example.com
    To: aperson@example.com
    MIME-Version: 1.0
    Content-Type: multipart/mixed; boundary="..."
    Message-ID: ...
    Date: ...
    Precedence: bulk
    <BLANKLINE>
    --...
    Content-Type: text/plain; charset="us-ascii"
    MIME-Version: 1.0
    Content-Transfer-Encoding: 7bit
    <BLANKLINE>
    [No bounce details are available]
    --...
    Content-Type: message/rfc822
    MIME-Version: 1.0
    <BLANKLINE>
    To: ant@example.com
    From: aperson@example.com
    Subject: Something important
    <BLANKLINE>
    I sometimes say something important.
    <BLANKLINE>
    --...--

An error message can be given when the message is bounced, and this will be
included in the payload of the ``text/plain`` part.  The error message must be
passed in as an instance of a ``RejectMessage`` exception.

    >>> from mailman.interfaces.pipeline import RejectMessage
    >>> error = RejectMessage("This wasn't very important after all.")
    >>> bounce_message(mlist, msg, error)
    >>> items = get_queue_messages('virgin', expected_count=1)
    >>> print(items[0].msg.as_string())
    Subject: Something important
    From: ant-owner@example.com
    To: aperson@example.com
    MIME-Version: 1.0
    Content-Type: multipart/mixed; boundary="..."
    Message-ID: ...
    Date: ...
    Precedence: bulk
    <BLANKLINE>
    --...
    Content-Type: text/plain; charset="us-ascii"
    MIME-Version: 1.0
    Content-Transfer-Encoding: 7bit
    <BLANKLINE>
    This wasn't very important after all.
    --...
    Content-Type: message/rfc822
    MIME-Version: 1.0
    <BLANKLINE>
    To: ant@example.com
    From: aperson@example.com
    Subject: Something important
    <BLANKLINE>
    I sometimes say something important.
    <BLANKLINE>
    --...--

The ``RejectMessage`` exception can also include a set of reasons, which will
be interpolated into the message using the ``{reasons}`` placeholder.

    >>> error = RejectMessage("""This message is rejected because:
    ...
    ... $reasons
    ... """, [
    ...     'I am not happy',
    ...     'You are not happy',
    ...     'We are not happy'])
    >>> bounce_message(mlist, msg, error)
    >>> items = get_queue_messages('virgin', expected_count=1)
    >>> print(items[0].msg.as_string())
    Subject: Something important
    From: ant-owner@example.com
    To: aperson@example.com
    MIME-Version: 1.0
    Content-Type: multipart/mixed; boundary="..."
    Message-ID: ...
    Date: ...
    Precedence: bulk
    <BLANKLINE>
    --...
    Content-Type: text/plain; charset="us-ascii"
    MIME-Version: 1.0
    Content-Transfer-Encoding: 7bit
    <BLANKLINE>
    This message is rejected because:
    <BLANKLINE>
    I am not happy
    You are not happy
    We are not happy
    <BLANKLINE>
    --...
    Content-Type: message/rfc822
    MIME-Version: 1.0
    <BLANKLINE>
    To: ant@example.com
    From: aperson@example.com
    Subject: Something important
    <BLANKLINE>
    I sometimes say something important.
    <BLANKLINE>
    --...
    <BLANKLINE>

.. _Wikipedia page: https://en.wikipedia.org/wiki/Bounce_message
