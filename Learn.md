# Read the doc more!, you cu**

>Doc need some more integration like on the first page there is no link for mailman client and other things (Will raise it in some thread or something).  
>Have not done this currenlty but will do.  
> Some updation regarding docs related to my proposal also needs to be done.

1. Mailman Client
    - >Official Way to talk to REST API of Mailman Core.

    - ~~Started to work on a issue regarding mailman client  with that I can also work on a postirous issue also(should do them fast).~~
    - ~~Send the pr ,integrartion failed have asked for help in the thread.~~
    - ~~Tests passed, understood.~~
    - He added some comments I gave replies, waiting for his.

2. ~~The links are depreciated many stating to use <http://localhost:9001/...>  in the docs (have started a thread on that).~~
    - Got a reply have understood that.

3. ~~Somethings like logs and all I think should be added to gitignore have started a thread.~~
    - Got a reply , understood that.

4. Start collecting data regarding project and see for other issue to solve.
    - See bouncing and related info in doc add a and detailed issue in the thread.
    - ~~Post doubts.~~
    - ~~Had posted elaborated status in thread Mark Saporo had replied.~~
    - ~~Got some nice info, written below.~~
    - Post more doubts.

5. They use `VERP` see their previous Mailman Documentation.
    - ~~Understand the files and start writing below.~~
    - ~~Understanding code and writiing under topic `Bounce processes` below.~~
    - ~~Understood most of it, will write more when I will work on it in the proposal.~~
    - Proposal done, see below for more links regarding verp or [this](https://berserker1.github.io/Mailman_GSoC_Org/) blog post of mine has a verp section.

6. See to solve more issues on mailman core, get to know a lot.

7. ~~Understand how their bounce msg is generated what is the content and all.~~
    - ~~Does it containg only one address or multiple addresses.~~
    - Do not need that now.

8. See where and what exactly is registered.
    - Still have to figure this out but moved to other priorities first.

9. ~~Learn how to wrap code in python (like `functools`, etc)~~
    - Got the basic idea.

10. **Learn about database and how mailman store things for your proposal**.
    - Got to know it uses SQLAlchemy for ORM, default RDMS is SQLite3 but supports others also like postgresql, also uses alembic for database migration ( if you do not understand these terms then google them ). Learn more!
    - Member and Mailing Lists have different models, you have to modify them and their interfaces also.
    - **Read about SQL Alchemy!!.**
      - Got basic understanding, now try to implement nicely.
    - ~~Docs do not have currently a comprehensive way to explain the models. Should ask a elaborate thing in the mailing list.~~
      - Asked this to mentor, can raise this more detailed in the mailing list.
    - ~~Posted various doubts in thread, waiting for reply.~~
      - Got replies, have understood that, now work more and repeat.
11. Started learning about basic `hyperkitty`.

12. Learned about `init.py` file, some basic stuff, learned it from the [doc](https://docs.python.org/3/reference/import.html#regular-packages) and from [this](https://medium.com/@ramrajchandradevan/python-init-py-modular-imports-81b746e58aae) article.

## Mailman models and interfaces stuff

[This](https://mailman.readthedocs.io/en/latest/src/mailman/docs/architecture.html#user-model) link explains the Mailman Core architecture, go through it first.

- First they create an interface and define certain attribute using **zope** ( Google that if you do not get it ), then they create models upon that interfaces.
- **Addresses** also have their own model and interface. They are linked to the **members** model.
- Technically Mailman has first entity named **User**. **User** repressent people in Mailman. After that according to the role the user can become:
  - Moderator
  - Administrator
    - They are a union of list's owners and administrators.
  - Member

## Bounce processes

>- File `bounce.py` in `mailman/src/mailman/interfaces` is an interface file which defines various class of bounce functions.Here the templates are to define a `registration record` and `register` bounce events.
>
>- File `bounce.py` in `mailman/src/mailman/model` is a model file for various functions, it works on the interfaces provided by the above files.
>
>- File `bounce.py` in `mailman/src/mailman/runners/bounce.py` runs the `bounce runner`.  
More info below link in the title **Imp doc having Runners of mailman**
>   - Currently it is just finding out the temporary and permanent failures and storing the addresses to be processed.
>
>- File `bounces.rst` in `mailman/src/mailman/app/docs/bounces.rst` shows how a bounce message is generated and send to the original sender , how you can also send the error reasons in a dict and it will all be showed to the sender in an organized way.
>
>- File `bounce.rst` in `mailman/src/mailman/model/docs`  shows how a bounce event is registered , it stores various info .
>
>- File `test_bounces.py` in `mailman/src/mailman/app/tests` tests the `app.bounces` funnctions.
>
>- File `test_bounce.py` in `mailman/src/mailman/model/tests` tests the `bounce model objects` .
>
>- File `test_bounce.py` in `mailman/src/mailman/runners/tests` tests the `bounce runners`.

## For verp see -

<https://wiki.list.org/DOC/So%20what%20is%20this%20VERP%20stuff>

<https://mailman.readthedocs.io/en/latest/src/mailman/mta/docs/verp.html>

---

## Imp doc having Runners and their workflow of Mailman

>Read this [link here](https://mailman.readthedocs.io/en/latest/src/mailman/runners/docs/OVERVIEW.html)

### Basic understanding

Read [this](https://mailman.readthedocs.io/en/latest/src/mailman/app/docs/bounces.html) link of the doc for understanding the bounce process with some examples

- So there are basically site-owners and site has multiple lists for posting and each list have some list-owners.

  - If somebody sends a mail to a specific list owners/moderators the bounce of that mail will be recorded by `-bounces` runner of the site-owners irrespective of the sender being a bot or a person.
    This is to keep identify false emails of various list-owners.

  - If somebody sends a mail to site owners their bounces will be directly dumped to `data/owners-bounces.mbox`
  - If somebody sends a mail to the one of the lists its bounce will be processed under `-bounces` runner of that list and bounces stored in `data/owners-bounces.mbox`

**More info about how previous mailman processed bounces is [here](https://wiki.list.org/DOC/Mailman%202.1%20List%20Administrators%20Manual)**

- If we do various processes with Mailman Core then the job is tedious but their are examples and explanations in the doc of Mailman Core (name is GNU Mailman), but we have nice wrappers made in the Mailmanclient and that doc should be referenced more when using the Mailman shell. [This](http://docs.mailman3.org/projects/mailmanclient/en/latest/src/mailmanclient/docs/using.html#mailing-lists) page of the doc is usually more referred to.

---

## Proposal

- ~~A draft is sent containing the idea only. Their reply will determine the schedule and other things.~~
  - ~~Got some suggestions, fixed them.~~
  - ~~Now have to work on timeline and all.~~
  - Propsal done, GSoC done now figure out how to pass.

## Side Notes

- **dump_json** func is in the documentation.py file we can import it by running  
`from mailman.testing.documentation import dump_json`

- Know how to configure your **Python Interpreter** for virtual env.

- **Method for not failing tests**
  - check your branch for test failures and then open a merge request. Have asked for where to add test.

- **`MailingList.process_bounces`** is a flag which specifies whether the list should be processed for bounces or not.

- In `_dispose` func of file `bounce.py` in `mailman/src/mailman/runners/` we use `StandardVERP().get_verp(mlist,msg)` not to extract actually but to find whether there exsists verp bounces or not.
If the length is greater than 0 then extract all the temporary and permanent failures by `all_failures(msg)` func.

- **Some important points**
  - Mailman accepts incoming messages from the MTA using the Local Mail Transfer Protocol (LMTP) interface.
  - Mailman passes all outgoing messages to the MTA using the Simple Mail Transfer Protocol (SMTP).

- **Regarding how Alembic is used with Mailman according to Abhilash**
  - When you first start mailman, it will create the database with the current state of models and ignore all migrations
  - When you stop mailman, make changes to the model, restart mailman. It will see the database and not do any upgrades until you create a new migration file.
  - Once you have created the migration file, the database will be updated to that level and then stopping and starting won't see anything.
  - So, what you did was already migrated the previous one, but edited the migration afterwards. It doesn't know that you edited the migration, it only knows that it has already applied the migration (identified by the hash listed in there). What you need to do is drop the database and then re-try and things would work like you expect
  - Just remove var/data
  - Or var/data/mailman.db
  - Delete the sqlite file
  - Relevant doc of alembic is [here](https://alembic.sqlalchemy.org/en/latest/tutorial.html) and of Mailman is [here](https://mailman.readthedocs.io/en/latest/src/mailman/docs/database.html), although the Mailman one some info from above is requireds.

- **For understanding different datatypes of the bounce attributes you can see the `mailman/src/mailman/styles/base.py` file**.

## Definitions

**Temporary or Permanent bounes**  
`Question`  

    What are "temporary and permanent failures" mentioned in "bounce.py" in "mailman/src/mailman/runners/"

`ANSWER`

    - A temporary failure is a 4xx (retryable) status and will cause mailman to retry periodically for a configured time (defaults I think are 15 minutes and 5 days). If the message is still not delivered after the 5 days or whatever time, it is considered a bounce at that point.

    - A permanent failure is a 5xx (failure) status and is recorded as a bounce. All 5xx failures or failure DSNs are considered bounces. "Soft" reasons like full mailbox or message looks like spam are not distinguished from "hard" reasons like no such user. Whether or not a failure is considered retryable is up to the receiving MTA.

---

## Imp thread

- This thread is good for clearing basics regarding bounce processes : [link](https://mail.python.org/archives/list/mailman-developers@python.org/thread/N7LB2VQN5MC7INXTP3N2CF7MJYSZQI3W/)
- [This](https://mailman.readthedocs.io/en/latest/src/mailman/config/docs/config.html) doc link is important for configuring the Mailman as a whole.
- For mail sever and MTA shit, look up [here](https://mailman.readthedocs.io/en/latest/src/mailman/docs/mta.html).
- See [this](https://mailman.readthedocs.io/en/latest/build/lib/mailman/model/docs/subscriptions.html#unsubscribing) thread for figuring out various `Unsubscription Policies` and `Subscription Policies`.

## Hyperkitty

- Archives emails, we basically have a mailman-hyperkitty plugin which forwards emails to it and it archives.
  - Need to learn more about this.
